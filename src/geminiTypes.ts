// Incomplete tyings for `gemini`, containing only the subset we use
export interface Gemini {
  on: (eventName: string, handler: Function) => void;
};

export interface GeminiResult {
  equal: boolean;
  suite: GeminiSuite;
  state: string;
  browserId: string;
};

export interface GeminiSuite {
  name: string;
  path: string[];
  id: number;
  browsers: string[];
  file: string;
  url: string;
  captureSelectors: string[];
  skipped: Array<{ comment: string; }>;
}
